#!/usr/bin/env bash

apt-get update
apt-get -y upgrade

apt-get -y install git

sudo apt-get install -y build-essential

curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

apt-get update && sudo apt-get install nodejs yarn
sudo npm install -g json-server

echo "cd /home/vagrant/truly-contacts" >> /home/vagrant/.profile

su vagrant << EOF

echo "cd /home/vagrant/truly-contacts" >> /home/vagrant/Envs/interns/bin/postactivate

cd /home/vagrant/truly-contacts/

yarn
EOF
