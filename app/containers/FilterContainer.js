import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSearch } from '../actions'
import InputSearch from '../components/input-search/InputSearch'

const mapStateToProps = state => ({
    inputText: state.filters.inputText
})

const mapDispatchToProps = dispatch => ({
    updateSearch: bindActionCreators(updateSearch, dispatch)
})

const FilterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(InputSearch)

export default FilterContainer
