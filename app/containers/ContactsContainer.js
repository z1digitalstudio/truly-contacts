import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { expandContact, fetchContacts, fetchContactsSuccess, fetchContactsFailure } from '../actions'
import Contacts from '../components/contacts/Contacts'

const getContacts = (contacts, inputText) => {
    return contacts.filter(contact => contact.number.includes(inputText) || (contact.name && contact.name.toLowerCase().includes(inputText.toLowerCase())))
}

const mapStateToProps = state => ({
    contacts: getContacts(state.contacts.contactsList.contacts, state.filters.inputText),
    expandedContact: state.expandedContact
})

const mapDispatchToProps = dispatch => ({
    expandContact: bindActionCreators(expandContact, dispatch),
    fetchContacts: () => {
        dispatch(fetchContacts()).then((response) => {
            !response.error ? dispatch(fetchContactsSuccess(response.payload.data)) : dispatch(fetchContactsFailure(response.payload.data));
        });
    }
})

const ContactsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Contacts)

export default ContactsContainer
