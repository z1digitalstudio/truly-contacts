import { connect } from 'react-redux'
import ContactForm from '../components/contact-form/ContactForm'
import * as formsNames from '../constants/Forms'

const mapStateToProps = (state, ownProps) => ({
    form: formsNames.NEW_CONTACT_FORM
})

const ContactFormContainer = connect(
    mapStateToProps
)(ContactForm)

export default ContactFormContainer
