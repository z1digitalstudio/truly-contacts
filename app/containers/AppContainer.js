import { connect } from 'react-redux'
import App from '../components/app/App'



const mapStateToProps = state => ({
    hasError: state.globalErrors.showGlobalError
})

const AppContainer = connect(
    mapStateToProps
)(App)

export default AppContainer
