const ROOT_URL = location.href.indexOf('localhost') > 0 ? 'http://localhost:3001/' : '/';

export const CONTACTS = ROOT_URL + 'contacts?_sort=name&_order=ASC'
