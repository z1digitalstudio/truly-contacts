import React, { Component, PropTypes } from 'react'

import './contactAvatar.scss'

class ContactAvatar extends Component {
    render() {
        const { name } = this.props
        let initials = name ? name.split(' ').slice(0, 2).map((name_step) => name_step[0]):':)'
        return (
            <div className="contactAvatar">
                <span className="contactAvatar__initials">{initials}</span>
            </div>
        );

    }
}

ContactAvatar.propTypes = {
    name: PropTypes.string
}

export default ContactAvatar
