import React from 'react'

const renderField = ({ input, label, type, meta: { touched, error, invalid } }) => (
    <div className={`floatedLabel ${touched ? (invalid ? 'is-invalid':'is-valid') : ''}`}>
        <input {...input}
               id={input.name}
               type={type}/>
        {touched && ((error && <p className="floatedLabel__error">{error}</p>))}
        <label className="control-label" htmlFor={input.name}>{label}</label>
    </div>
)

export default renderField
