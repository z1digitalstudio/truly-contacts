import React, { Component, PropTypes } from 'react'
import { parse } from 'libphonenumber-js'

class PhoneLabel extends Component {
    render() {
        const { phone } = this.props
        let phoneFormatted = parse(phone)

        if (phoneFormatted.phone) {
            return (
                <a href={'tel:'+phone}
                   onClick={(e) => e.stopPropagation()}
                   className="contactCard__number">({phoneFormatted.country}) {phoneFormatted.phone}</a>
            )
        } else {
            return (
                <a href={'tel:'+phone}
                   onClick={(e) => e.stopPropagation()}
                   className="contactCard__number">{phone}</a>
            )
        }
    }
}

PhoneLabel.propTypes = {
    phone: PropTypes.string.isRequired
}

export default PhoneLabel
