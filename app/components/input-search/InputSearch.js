import React, { PropTypes, Component } from 'react';

import './inputSearch.scss'
import IconSearchInline from './iconSearch.inline.svg'
import IconResetInline from './iconReset.inline.svg'

class InputSearch extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.updateSearch(e.target.value || '');
    }

    renderClearButton() {
        return (
            <button
                className="inputSearch__reset"
                type="button"
                onClick={this.handleChange}><IconResetInline /></button>
        )
    }

    render() {
        const {inputText} = this.props
        return (
            <header className='inputSearch'>
              <label title="Search Contacts" className='inputSearch__form'>
                <IconSearchInline className='inputSearch__icon'/>
                <input
                    type="text"
                    value={inputText}
                    onChange={this.handleChange}
                    placeholder="Search..."
                />
                {inputText ? this.renderClearButton():null}
              </label>
            </header>
        );
    }
}

InputSearch.propTypes = {
    inputText: PropTypes.string,
    updateSearch: PropTypes.func.isRequired
}

export default InputSearch
