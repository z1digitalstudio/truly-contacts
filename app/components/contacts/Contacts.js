import React, { Component, PropTypes } from 'react'
import ContactsList from '../contacts-list/ContactsList'
import ContactsInfo from '../contacts-info/ContactsInfo'

class Contacts extends Component {
    componentWillMount() {
        this.props.fetchContacts();
    }

    render() {
        const { contacts, expandContact, expandedContact, fetchContacts } = this.props

        return (
            <main>
                <ContactsList
                    contacts={contacts}
                    onContactClick={expandContact}
                    expandedContact={expandedContact} />
                <ContactsInfo
                    totalContacts={contacts.length}
                    fetchContacts={fetchContacts}/>
            </main>
        );

    }
}

Contacts.propTypes = {
    contacts: PropTypes.array.isRequired,
    expandContact: PropTypes.func.isRequired,
    expandedContact: PropTypes.object.isRequired,
    fetchContacts: PropTypes.func.isRequired
}

export default Contacts
