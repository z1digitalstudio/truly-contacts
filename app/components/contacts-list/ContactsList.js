import React, { Component, PropTypes } from 'react'
import ContactRow from '../contact-row/ContactRow'

import './contactsList.scss'

class ContactsList extends Component {
    render() {
        const { contacts, onContactClick, expandedContact } = this.props

        return (
            <ul className="contactsList">
                {contacts.map(contact =>
                    <ContactRow
                        key={contact.number}
                        contact={contact}
                        onClick={() => onContactClick(contact.number === expandedContact.number ? '':contact.number)}
                        expanded={contact.number === expandedContact.number}
                    />
                )}
            </ul>
        );
    }
}

ContactsList.propTypes = {
    contacts: PropTypes.array.isRequired,
    onContactClick: PropTypes.func.isRequired,
    expandedContact: PropTypes.object.isRequired
}

export default ContactsList
