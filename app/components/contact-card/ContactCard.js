import React, { Component, PropTypes } from 'react'
import ContactAvatar from '../contact-avatar/ContactAvatar'
import PhoneLabel from '../phone-label/PhoneLabel'

import './contactCard.scss'

class ContactRow extends Component {
    render() {
        const { contact } = this.props

        return (
            <div className="contactCard">
                <ContactAvatar name={contact.name} />
                <div className="contactCard__content">
                    <p className="contactCard__context">{contact.context}</p>
                    <h1 className="contactCard__name">{contact.name}</h1>
                    <PhoneLabel phone={contact.number} />
                </div>
            </div>
        );

    }
}

ContactRow.propTypes = {
    contact: PropTypes.object.isRequired
}

export default ContactRow
