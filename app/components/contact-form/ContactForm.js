import React, { Component, PropTypes } from 'react'
import { reduxForm, Field, SubmissionError } from 'redux-form'
import renderField from '../render-field/renderField'
import { createContact, createContactSuccess, createContactFailure } from '../../actions'
import { isValidNumber } from 'libphonenumber-js'

import './contactForm.scss'

//TODO: refactor to field-level validation http://redux-form.com/6.4.0/examples/fieldLevelValidation/
function validate(values) {
    const errors = {};

    if (!values.name || values.name.trim() === '') {
        errors.name = 'Enter a Name';
    }
    if (!values.number || values.number.trim() === '') {
        errors.number = 'Enter a valid phone';
    } else if(!isValidNumber(values.number)) {
        errors.number = 'Remember country code!';
    }
    if (!values.context || values.context.trim() === '') {
        errors.context = 'Enter some content';
    }

    return errors;
}

const normalizePhone = (value) => {
    if (!value) {
        return value
    }
    const onlyNums = value.replace(/[^\d]/g, '')
    return `+${onlyNums}`
}

function validateAndCreateContact(values, dispatch, toggleNewForm, fetchContacts) {
    return dispatch(createContact(values))
        .then(result => {
            if (result.payload.response && result.payload.response.status !== 200) {
                dispatch(createContactFailure(result.payload.response.data));
                throw new SubmissionError(result.payload.response.data);
            } else {
                dispatch(createContactSuccess(result.payload.data));
                toggleNewForm();
                fetchContacts();
            }
        });
}

class ContactForm extends Component {
    render() {
        const {handleSubmit, submitting, toggleNewForm, fetchContacts } = this.props
        return (
            <form
                onSubmit={ handleSubmit((values, dispatch)=>{validateAndCreateContact(values, dispatch, toggleNewForm, fetchContacts)})}>
                <h1 className="newContact__title">Create New Contact</h1>
                <Field
                    label="Contact Name"
                    type="text"
                    name="name"
                    component={renderField} />
                <Field
                    name="number"
                    type="text"
                    normalize={normalizePhone}
                    component={renderField}
                    label="Phone number" />
                <Field
                    name="context"
                    type="text"
                    component={renderField}
                    label="Context" />
                <button
                    type="submit"
                    className="newContact__submit"
                    disabled={ submitting }>
                    Save Contact
                </button>
            </form>
        );
    }
}

ContactForm.propTypes = {
    form: PropTypes.string.isRequired,
    toggleNewForm: PropTypes.func.isRequired,
    fetchContacts: PropTypes.func.isRequired
}

export default reduxForm({
    validate
})(ContactForm)
