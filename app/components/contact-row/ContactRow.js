import React, { Component, PropTypes } from 'react'
import ContactCard from '../contact-card/ContactCard'

import './contactRow.scss'

class ContactRow extends Component {
    renderContactCard(contact, expanded) {
        if (expanded) {
            return (
              <div className='contactRow__panel'>
                <ContactCard contact={contact}/>
              </div>
            )
        }
    }
    render() {
        const { contact, onClick, expanded } = this.props
        let state = expanded ? 'contactRow is-expanded':'contactRow'

        return (
            <li onClick={onClick} className={state}>
              <p className='contactRow__summary'>
                {contact.name}
              </p>
              { this.renderContactCard(contact, expanded) }
            </li>
        );

    }
}

ContactRow.propTypes = {
    contact: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
    expanded: PropTypes.bool
}

export default ContactRow
