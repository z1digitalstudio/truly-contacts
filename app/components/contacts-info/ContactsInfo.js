import React, { Component, PropTypes } from 'react'
import ContactFormContainer from '../../containers/ContactFormContainer'

import './contactsInfo.scss'
import IconAddInline from './iconAdd.inline.svg'

class ContactsInfo extends Component {
    constructor () {
        super()
        this.state = {
            showNewContact: false
        };
    }

    toggleNewForm () {
        const active = !this.state.showNewContact;
        this.setState({ showNewContact: active });
    }

    render() {
        const { totalContacts, fetchContacts } = this.props
        const showNewContactClass = this.state.showNewContact ? 'is-visible':''

        return (
        <div className={'newContact ' + showNewContactClass}>
            <button
                type="button"
                className="newContact__toggle"
                onClick={() => this.toggleNewForm()}><IconAddInline /></button>
            <div className="newContact__form">
                { this.state.showNewContact ? <ContactFormContainer toggleNewForm={() => this.toggleNewForm()} fetchContacts={fetchContacts}/>:false }
            </div>
            <p className="contactsInfo">{totalContacts} contacts</p>
        </div>
        );
    }
}

ContactsInfo.propTypes = {
    totalContacts: PropTypes.number,
    fetchContacts: PropTypes.func.isRequired
}

export default ContactsInfo
