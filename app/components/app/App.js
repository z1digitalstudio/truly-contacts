import React, { Component, PropTypes } from 'react'
import FilterContainer from '../../containers/FilterContainer'
import ContactsContainer from '../../containers/ContactsContainer'

class App extends Component {
    render() {
        const { hasError } = this.props
        let state = hasError ? 'mainSection has-error':'mainSection'

        return (
            <section className={state}>
                <FilterContainer />
                <ContactsContainer />
            </section>
        );
    }
}

App.propTypes = {
    hasError: PropTypes.bool
}

export default App
