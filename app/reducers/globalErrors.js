import * as types from '../constants/ActionTypes'

const INITIAL_STATE = {
        showGlobalError: false
    }

const globalErrors = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.FETCH_CONTACTS_FAILURE:
            return {
                showGlobalError: true
            };
        case types.CREATE_CONTACT_FAILURE:
            return {
                showGlobalError: true
            };
        default:
            return state
    }
}

export default globalErrors
