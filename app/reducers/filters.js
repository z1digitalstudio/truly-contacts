const DEFAULT_STATE = {
    inputText: ''
};

const filters = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case 'UPDATE_SEARCH':
            return { ...state, inputText: action.inputText }
        default:
            return state
    }
};

export default filters