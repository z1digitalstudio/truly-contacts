const DEFAULT_STATE = {
    number: ''
};

const expandedContact = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case 'EXPAND_CONTACT':
            return { ...state, number: action.number }
        default:
            return state
    }
}

export default expandedContact