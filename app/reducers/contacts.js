import * as types from '../constants/ActionTypes'

const INITIAL_STATE = {
        contactsList: {
            contacts: [],
            error: null,
            loading: false
        },
        //TODO: we are not using newContact currently because server return always 500 when fail
        newContact: {
            contact: null,
            error: null,
            loading: false
        }
    }

const contacts = (state = INITIAL_STATE, action) => {
    let error
    switch (action.type) {
        case types.FETCH_CONTACTS:
            return {
                ...state,
                contactsList: {
                    contacts: [],
                    error: null,
                    loading: true
                }
            };
        case types.FETCH_CONTACTS_SUCCESS:
            return {
                ...state,
                contactsList: {
                    contacts: action.payload,
                    error: null,
                    loading: false
                }
            };
        case types.FETCH_CONTACTS_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {
                ...state,
                contactsList: {
                    contacts: [],
                    error: error,
                    loading: false
                }
            };
        case types.CREATE_CONTACT:
            return {
                ...state,
                newContact: {
                    ...state.newContact,
                    loading: true
                }
            };
        case types.CREATE_CONTACT_SUCCESS:
            //FIXME: this should add new contact to contacts instead call server again
            return {
                ...state,
                newContact: {
                    contact: action.payload,
                    error: null,
                    loading: false
                }
            };
        case types.CREATE_CONTACT_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {
                ...state,
                newContact: {
                    contact: null,
                    error: error,
                    loading: false
                }
            };
        default:
            return state
    }
}



export default contacts