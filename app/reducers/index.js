import { combineReducers } from 'redux'
import contacts from './contacts'
import filters from './filters'
import expandedContact from './expandedContact'
import globalErrors from './globalErrors'
import { reducer as formReducer } from 'redux-form'

const contactApp = combineReducers({
    contacts,
    expandedContact,
    filters,
    globalErrors,
    form: formReducer
});

export default contactApp
