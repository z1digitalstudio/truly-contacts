import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import promise from 'redux-promise'
import contactsApp from './reducers'
import AppContainer from './containers/AppContainer'

if(process.env.NODE_ENV !== 'production') {
    React.Perf = require('react-addons-perf');
}

//Configure middleware w/ redux-promise for AJAX requests
const createStoreWithMiddleware = applyMiddleware(
    promise
)(createStore);
const store = createStoreWithMiddleware(contactsApp);

render(
    <Provider store={store}>
        <AppContainer />
    </Provider>,
    document.getElementById('app')
)
