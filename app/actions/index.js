import axios from 'axios'
import * as types from '../constants/ActionTypes'
import * as API from '../constants/API'

export const updateSearch = inputText => ({ type: types.UPDATE_SEARCH, inputText })
export const expandContact = number => ({ type: types.EXPAND_CONTACT, number })

export function fetchContacts() {
    const request = axios({
        method: 'get',
        url: API.CONTACTS,
        headers: []
    });

    return {
        type: types.FETCH_CONTACTS,
        payload: request
    };
}
export const fetchContactsSuccess = contacts => ({type: types.FETCH_CONTACTS_SUCCESS, payload: contacts})
export const fetchContactsFailure = error => ({type: types.FETCH_CONTACTS_FAILURE, payload: error})

export function createContact(props) {
    const request = axios({
        method: 'post',
        data: props,
        url: API.CONTACTS,
    });

    return {
        type: types.CREATE_CONTACT,
        payload: request
    };
}
export const createContactSuccess = newContact => ({type: types.CREATE_CONTACT_SUCCESS, payload: newContact})
export const createContactFailure = error => ({type: types.CREATE_CONTACT_FAILURE, payload: error})
