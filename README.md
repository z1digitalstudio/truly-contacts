# Commite’s Truly Test
A simple app that interacts with an API and displays data in a usable fashion.

## Scope
The main goal is to create a simple contact management app with an appealing visual layer and intuitive interactions.

## Design Process
The application has been created following the [Design Sprint](https://www.gv.com/sprint/) methodology with some modifications in time and scope.

Firstly, the **ideation phase** considered all the interesting functionality that a contact management application should have.

Then, the **definition phase** delivered the UI design for the full-scope application.

![Definition Phase](https://gitlab.com/commite/truly-contacts/raw/master/docs/definition.jpg)

After that, in the **decision phase**, the scope of the application was limited by the requirements of the test.

![Decision Phase](https://gitlab.com/commite/truly-contacts/raw/master/docs/decision.jpg)

Finally, in the **prototyping and testing phases**, the application was implemented and tested respectively.

## Development
The development environment is based on [Vagrant](https://www.vagrantup.com) for virtualization and [Webpack](http://webpack.github.io) for dependencies management and building process.

### Front-End Logic
The interface functionality has been implemented with [React](https://facebook.github.io/react/) following the Presentational-Container Pattern. Also, the following dependencies have been included in order to meet the project needs:

 * Redux
 * Redux-Form
 * Axios

 **Considerations:** Since the REST API is based on JSON-Server all the server errors are mapped as a general 500 error.

### Visual Layer
The interface design has been implemented using the [SASS](http://sass-lang.com) preprocessor with SCSS syntax and following the [BEM](http://getbem.com/introduction/) naming and methodology.

Each component has been crafted independently and orchestrated following a templating approach, similar to the [Atomic Design](http://atomicdesign.bradfrost.com) paradigm.

Font sizing and white space are tied to a 4px-grid that enhances the vertical rhythm and the overall balance of the design.

The implementation has followed a mobile-first approach that adapts the interface components to the viewport size responsively.

## Running the Application
After cloning the code from the repository and setting Vagrant up, the application can be launched in a few steps.

### Vagrant Initialization:
```
vagrant up --provision
```
**Note**: The *provision* parameter is only needed the first time.

### Start API Server
```
vagrant ssh
yarn start-api
```

### Start Application
Once the API server is running, the application has to be launched in another process.

```
vagrant ssh
yarn start
```

The application is now accessible in http://localhost:3000.
